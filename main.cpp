/*******************************************************************
   Hamed Mirzaei
   telegram  ID --> @HamedMirzaei_Official
   instagram ID --> HamedMirzaei2001Official
   email --> HamedMirzaei2001Official@gmail.com
********************************************************************/

#include <iostream>
#include<windows.h>
using namespace std;

//prototype of functions
int make_grades(int[], int[], int[], int);
void Soretd(int[], int[], int);

int main()
{
    // size of students that attention in this exam
    int student_size = 0;
    // size of question that each student should answer it in this exam
    int question_size = 0;
    //dynamic array of true answers that master Enter it
	int* answer_key = new int[question_size];
    // dynamic array of score of each question that master Enter it
    int* score_of_question = new int[question_size];
    // dynamic array of answers that student Enter it
    int* student_answer = new int[question_size];
    // dynamic array of student code that each student Enter it for attention to exam
    int* student_code = new int[student_size];
    // dynamic array of grades of students that attention in this exam and answer the question
    int* grades = new int[student_size];
    // a variable for control while loop
    int counter = 0;

    //20-46 for master of school that should Enter it
    cout<<"Enter Number of Student that attention in this exam : ";
    cin>>student_size;


    cout<<"Enter Number of question in this exam : ";
    cin>>question_size;

    system("cls");

    cout<<"Enter keys of question : \n";
    for(int i=0; i<question_size; i++)
        cin>>answer_key[i];

    system("cls");

    cout<<"Enter score of each question in order : \n";
    for(int i=0; i<question_size; i++)
        cin>>score_of_question[i];

    system("cls");

    // loop recur one time for each student
    while(counter<student_size)
    {
        system("cls");
        cout<<"student "<<counter+1<<" Enter your student code : ";
        cin>>student_code[counter];
        cout<<"your exam started\n\n";

        cout<<"Enter your answers (0 == you don't answer the question):\n";
        for(int i=0; i<question_size; i++)
            cin>>student_answer[i];


        grades[counter] = make_grades(student_answer, answer_key, score_of_question, question_size);

        cout <<"finish!!!\n\nyour grade is : "<<grades[counter]<<endl;
        Sleep(500);
        counter++;
    }

    Soretd(student_code, grades, student_size);


	return 0;
}

// function that count grade of each student
int make_grades(int student_answer[], int answer_key[], int score_of_question[], int size)
{

	int positive = 0, notattempt = 0;

	for (int i = 0; i < size; i++)
    {
		if (student_answer[i] == 0)
			notattempt++;

		else if (answer_key[i] == student_answer[i])
			positive += score_of_question[i];


	}

	return positive;

}


void Soretd(int codes[], int grades[], int size)
{
    for(int i=0; i<size; i++)
    {
        for(int j=0; j<size-i-1; j++)
        {
            if(grades[j] < grades[j+1])
            {
                int temp1 = grades[j+1];
                grades[j+1] = grades[j];
                grades[j] = temp1;

                int temp2 = codes[j+1];
                codes[j+1] = codes[j];
                codes[j] = temp2;

            }
        }
    }
    system("cls");
    cout<<"list of student in order grades(down) :\n";
    for(int i=0; i<size; i++)
        cout<<i+1<<". student with code : "<<codes[i]<<"     grade : "<<grades[i]<<endl;

}
